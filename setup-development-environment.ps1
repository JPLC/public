# Download Script for QA_Box
# Author: Daniel Mirota
# Date: 2013-07-08
#
# Portions of this code from https://chocolatey.org/install.ps1
Write-Host ""

# From http://stackoverflow.com/questions/5466329/whats-the-best-way-to-determine-the-location-of-the-current-powershell-script
$scriptPath = Get-Location

$launcher = Join-Path $scriptPath "setup-environment.bat"

if (Test-Path $launcher){
    Remove-Item $launcher
}

$stream = [System.IO.StreamWriter] $launcher
$stream.WriteLine("@echo off")

# Download cmake
$version = "cmake-2.8.11.2-win32-x86"
$url = "http://www.cmake.org/files/v2.8/$version.zip"
$file = Join-Path $scriptPath "cmake.zip"
$dir = Join-Path $scriptPath "cmake"

if (![System.IO.Directory]::Exists($dir)) {
    Write-Host "Creating Directory $dir"
    [System.IO.Directory]::CreateDirectory($dir)
}

if (![System.IO.File]::Exists($file)){
    Write-Host "Downloading $url to $file"
    $downloader = new-object System.Net.WebClient
    $downloader.DownloadFile($url, $file)
}

# unzip the package
$directoryInfo = Get-ChildItem $dir | Measure-Object # from http://stackoverflow.com/questions/10550128/powershell-test-if-folder-empty

if ($directoryInfo.count -eq 0){
	Write-Host "Extracting $file to $dir..."
	$shellApplication = new-object -com shell.application 
	$zipPackage = $shellApplication.NameSpace($file) 
	$destinationFolder = $shellApplication.NameSpace($dir) 
	$destinationFolder.CopyHere($zipPackage.Items(),0x10)
}

# Add cmake to the path
$path = Join-Path $dir $version
$path = Join-Path $path "bin"

$env:Path += ";$path"
$stream.WriteLine("for %%X in (cmake.exe) do (set FOUND=%%~`$PATH:X)")
$stream.WriteLine("if not defined FOUND Path %PATH%;$path")
$stream.WriteLine("set FOUND=")


# Download 7-zip
$version = "7z922"
$url = "http://downloads.sourceforge.net/project/sevenzip/7-Zip/9.22/$version.msi"
$file = Join-Path $scriptPath "7zip.msi"
$dir = Join-Path $scriptPath "7zip"

if (![System.IO.Directory]::Exists($dir)) {
    Write-Host "Creating Directory $dir"
    [System.IO.Directory]::CreateDirectory($dir)
}

if (![System.IO.File]::Exists($file)){
    Write-Host "Downloading $url to $file"
    $downloader = new-object System.Net.WebClient
    $downloader.DownloadFile($url, $file)
 }

# unzip the package
$directoryInfo = Get-ChildItem $dir | Measure-Object
if ($directoryInfo.count -eq 0){
	Write-Host "Extracting $file to $dir..."
	msiexec /a $file /qn TARGETDIR=$dir
}


$sevenzip_executable = Join-Path $scriptPath "/7zip/Files/7-Zip/7z.exe"

# Download git
$version = "PortableGit-1.8.3-preview20130601"
$url = "https://msysgit.googlecode.com/files/$version.7z"
$file = Join-Path $scriptPath "git.7z"
$dir = Join-Path $scriptPath "git"

if (![System.IO.Directory]::Exists($dir)) {
    Write-Host "Creating Directory $dir"
    [System.IO.Directory]::CreateDirectory($dir)
}

if (![System.IO.File]::Exists($file)){
    Write-Host "Downloading $url to $file"
    $downloader = new-object System.Net.WebClient
    $downloader.DownloadFile($url, $file)
}

# unzip the package
$directoryInfo = Get-ChildItem $dir | Measure-Object
if ($directoryInfo.count -eq 0){
	Write-Host "Extracting $file to $dir..."
	$extract_git = "$sevenzip_executable x -o$dir -y $file"
	Invoke-Expression $extract_git
}

# Add git to the path
$git_install_root = $dir
$path1 = Join-Path $git_install_root "bin"
$path2 = Join-Path $git_install_root "mingw\bin"
$path3 = Join-Path $git_install_root "cmd"
$env:Path = "$path1;$path2;$path3;$env:Path"
$stream.WriteLine("for %%X in (git.exe) do (set FOUND=%%~`$PATH:X)")
$stream.WriteLine("if not defined FOUND Path $path1;$path2;$path3;%PATH%")



# create the source directory
$dir = Join-Path $scriptPath "QA_Box"
if (![System.IO.Directory]::Exists($dir)) {
    Write-Host "Creating Directory $dir"
    [System.IO.Directory]::CreateDirectory($dir)
}

$git_dir = Join-Path $dir ".git" 

# checkout repo into source directory
if (![System.IO.Directory]::Exists($git_dir)) {
    git clone https://bitbucket.org/JPLC/QA_Box.git $dir
}else{
#or update the the source directory
    Set-Location -Path $dir
    git rebase master
}

# create the build directory
$dir = Join-Path $scriptPath "QA_Box-build"
if (![System.IO.Directory]::Exists($dir)) {
    Write-Host "Creating Directory $dir"
    [System.IO.Directory]::CreateDirectory($dir)
}

# configure the project
Set-Location -Path $dir
cmake ../QA_Box -G "NMake Makefiles"

$stream.close()

# nmake launch with_jom
nmake with_jom

